<?php
require_once 'vendor/autoload.php';
include_once 'setting.php'; //Loading settings for db
Twig_Autoloader::register(); /*Twig activation*/

/**
 * This method check URL
 */
if (strlen($_SERVER['REQUEST_URI']) > 1) {
    //clear url
    $page = substr($_SERVER['REQUEST_URI'], 1);
    if (!preg_match('/^[A-z0-09]{3,255}/', $page)) {
        exit('error url');
    }
    if ($page{strlen($page) - 1} == '/') {
        $page = substr($page, 0, -1);
    }

}
/**
 * Session start and mysql connect
 */
session_start();
$CONNECT = mysqli_connect(HOST, USER, PASS, DB);

/**
 * Check user, in login?
 */
$loader = new Twig_Loader_Filesystem('templates');
$twig   = new Twig_Environment($loader, ['debug' => true]);
if ($_GET['quit']) {
    session_destroy();
    $template = $twig->loadTemplate('index.html');
    echo $template->render([]);
    exit();
}


if (!$_SESSION['login']) {
    loginUser($CONNECT);
    $template = $twig->loadTemplate('index.html');
    echo $template->render([]);
    exit();
} else {
    loginUser($CONNECT);
    addNewUser($page,$CONNECT,$twig);
    updateUser($page, $CONNECT, $twig);
    listUsers($page, $CONNECT, $twig);
    /* Render page based on the url */
    $page = $page ? $page : 'index';
    if ($page != 'index') {
        $page = '404';
    }
    $template = $twig->loadTemplate($page . '.html');
    echo $template->render([]);
    exit();

}

/**
 * This function for user login
 * @param $CONNECT
 */
function loginUser($CONNECT)
{
    if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['action_login']) {
        $data     = $_POST;
        $userName = null;
        $password = null;
        $remember = null;
        if (!empty($data)) {
            if (!empty($data['username'])) $userName = $data['username'];
            if (!empty($data['password'])) $password = $data['username'];
            if (!empty($data['remember'])) $remember = $data['remember'];
            if ($remember) {
                ini_set('session.cookie_lifetime', 0);
            }
        }


        $fields = [
            $userName => 'login',
            $password => 'password',
        ];

        foreach (array_keys($fields) as $key) {
            if (validationMethod($fields[$key], $key)) {
                exit(json_encode([
                    'status' => 'error',
                    'text'   => validationMethod($fields[$key], $key)
                ]));
            }
        }

        $sql  = mysqli_query($CONNECT, 'SELECT `login`,`password` from `users` WHERE `login` = "' . $userName . '"');
        $user = mysqli_fetch_array($sql);
        if (!$user) {
            $json_data = [
                'status' => 'error',
                'text'   => 'Invalid Username or Password'
            ];
            exit(json_encode($json_data));
        } else {
            if ($user['login'] == $userName && $user["password"] == md5($password)) {
                $_SESSION['login'] = true;
                exit(json_encode(['stats' => 'ok']));
            }
        }
        $json_data = [
            'status' => 'error',
            'text'   => 'Invalid Username or Password'
        ];
        exit(json_encode($json_data));
    }

}

/**
 * This function for add new user in db
 * @param $page
 * @param $CONNECT - mysql connect
 * @param $twig
 */
function addNewUser($page,$CONNECT,$twig)
{
    if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['add_user']) {
        $data     = $_POST;
        $userName = null;
        $password = null;
        $fName    = null;
        $lName    = null;
        $gender   = null;
        $date     = null;
        if (!empty($data)) {
            if (!empty($data['login'])) $userName    = $data['login'];
            if (!empty($data['password'])) $password = $data['password'];
            if (!empty($data['fname'])) $fName       = $data['fname'];
            if (!empty($data['lname'])) $lName       = $data['lname'];
            if (!empty($data['gender'])) $gender     = $data['gender'];
            if (!empty($data['date'])) $date         = $data['date'];
        }

        $fields = [
            $userName => 'login',
            $password => 'password',
            $fName    => 'first name',
            $lName    => 'last name'
        ];

        foreach (array_keys($fields) as $key) {
            if (validationMethod($fields[$key], $key)) {
                exit(json_encode([
                    'status' => 'error',
                    'text'   => validationMethod($fields[$key], $key)
                ]));
            }
        }

        $sql  = mysqli_query($CONNECT, 'SELECT `login` from `users` WHERE `login` = "' . $userName . '"');
        $user = mysqli_fetch_array($sql);
        if ($user) {
            $json_data = [
                'status' => 'error',
                'text'   => 'This user exists'
            ];
            exit(json_encode($json_data));
        } else {
            $password = md5($password);
            $date     = date("Y-m-d", strtotime($date));               //(`id`,    `login`,        `password`,      `fname`,      `lname`,      `gender`,  `date_of_birth`, `rights`)
            mysqli_query($CONNECT, "INSERT INTO `users`  VALUES ('0', '" . $userName . "', '" . $password . "', '" . $fName . "', '" . $lName . "', '" . $gender . "', '" . $date . "', NULL);");
            exit(json_encode([
                'stats' => 'ok',
                'text'  => 'User added successfully '
            ]));
        }
        exit(json_encode(['stats' => 'ok']));
    }
    if (strpos($page, 'add_user') !== false) {
        $template = $twig->loadTemplate('add_user.html');
        echo $template->render([]);
        exit();
    }
}

/**
 * this function update user data
 * @param $page - page base url
 * @param $CONNECT - link mysql
 * @param $twig - twig
 */
function updateUser($page, $CONNECT, $twig)
{
    $goBack = getenv("HTTP_REFERER");
    /*if page update*/
    if (strpos($page, 'update') !== false) {
        $id = explode('/', $page);
        if (array_key_exists(1, $id)) {
            $id  = $id[1];
            if (strpos($goBack, 'update/' . $id) !== false) $goBack = '';
            $sql      = mysqli_query($CONNECT, 'SELECT * from `users` WHERE `id` = "' . $id . '"');
            $user     = mysqli_fetch_array($sql);
            $date     = date("d-m-Y", strtotime($user['date_of_birth']));
            $template = $twig->loadTemplate('update.html');
            echo $template->render([
                'id'       => $user['id'],
                'username' => $user['login'],
                'fname'    => $user['fname'],
                'lname'    => $user['lname'],
                'gender'   => $user['gender'],
                'date'     => $date,
                'back'     => $goBack,
                'url'      => 'update/' . $id
            ]);
            exit();
        }
    }
    /**
     * Post method upadte User
     */
    if ($page == 'update' && $_POST['update_user'] && $_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['id'];
        if ($id) {
            $sql  = mysqli_query($CONNECT, 'SELECT * from `users` WHERE `id` = "' . $id . '"');
            $user = mysqli_fetch_array($sql);
            if (!$user) {
                $json_data = [
                    'status' => 'error',
                    'text'   => 'User not exist'
                ];
                exit(json_encode($json_data));
            }

            $data     = $_POST;
            $id       = null;
            $userName = null;
            $password = null;
            $fName    = null;
            $lName    = null;
            $gender   = null;
            $date     = null;

            if (!empty($data)) {
                if (!empty($data['id'])) $id          = $data['id'];
                if (!empty($data['login'])) $userName = $data['login'];
                if (!empty($data['fname'])) $fName    = $data['fname'];
                if (!empty($data['lname'])) $lName    = $data['lname'];
                if (!empty($data['gender'])) $gender  = $data['gender'];
                if (!empty($data['date'])) $date      = date("Y-m-d", strtotime($data['date']));
            }

            $fields = [
                $userName => 'login',
                $fName    => 'first name',
                $lName    => 'last name'
            ];

            foreach (array_keys($fields) as $key) {
                if (validationMethod($fields[$key], $key)) {
                    exit(json_encode([
                        'status' => 'error',
                        'text'   => validationMethod($fields[$key], $key)
                    ]));
                }
            }

            $query = "UPDATE `users` SET `login` = '" . $userName . "', `fname` = '" . $fName . "', `lname` = '" . $lName . "', `gender` = '" . $gender . "', `date_of_birth` = '" . $date . "' WHERE `users`.`id` = " . $id . ";";
            $sql   = mysqli_query($CONNECT, $query);
            mysqli_fetch_array($sql);
            if (!mysqli_error($CONNECT)) {
                $json_data = [
                    'status' => 'ok',
                    'text'   => 'User has been updated'
                ];

            } else {
                $json_data = [
                    'status' => 'error',
                    'text'   => mysqli_error($CONNECT)
                ];
            }
            exit(json_encode($json_data));
        }
    }
}


/**
 * this function update user data
 * @param $page - page base url
 * @param $CONNECT - link mysql
 * @param $twig - twig
 */
function listUsers($page, $CONNECT, $twig)
{

    /*if page update*/
    if (strpos($page, 'list') !== false) {
        setPage();
        setMaxLimit();
        $sql       = mysqli_query($CONNECT, 'SELECT COUNT(*) FROM `users`');
        $max_users = current(mysqli_fetch_array($sql));
        $pgn       = pagination($max_users, $_GET['page'] ? $_GET['page'] : 1);
        $query     = 'SELECT * from `users`';

        if (array_key_exists('orderby', $_GET)) {
            $query .= 'ORDER BY ' . $_GET['orderby'];
            if ($_SESSION['last_order_by'] == $_GET['orderby'] . '_asc') {
                $query .= ' DESC';
                $_SESSION['last_order_by'] = $_GET['orderby'] . '_desc';
            } else {
                $query .= ' ASC';
                $_SESSION['last_order_by'] = $_GET['orderby'] . '_asc';
            }
        } else {
            $_SESSION['last_order_by'] = 'id_asc';
            $query .= 'ORDER BY id ASC';
        }
        $query .= " LIMIT " . $pgn['current'] . "," . $pgn['show'];
        $sql   = mysqli_query($CONNECT, $query);
        $users = mysqli_fetch_all($sql, MYSQLI_ASSOC);

        $template = $twig->loadTemplate('list.html');
        echo $template->render([
            'users' => $users,
            'png'   => $pgn,
            'order' => $_SESSION['last_order_by'],
        ]);
        exit();
    }
}

function setPage()
{
    if ($_GET['page'] > 0 && $_GET['page'] != $_SESSION['page']) {
        $_SESSION['page'] = $_GET['page'];
    }
    if (!$_GET['page'] && !$_SESSION['page']) {
        $_SESSION['page'] = 1;
    }
}

function setMaxLimit()
{
    if ($_GET['limit'] > 0 && $_GET['limit'] != $_SESSION['max_show']) {
        $_SESSION['max_show'] = $_GET['limit'];
    }
    if (!$_GET['limit'] && !$_SESSION['max_show']) {
        $_SESSION['max_show'] = 3;
    }
}

function pagination($max_users, $page)
{
    $max_show   = $_SESSION['max_show'];
    $limit_page = ceil($max_users / $max_show);
    $current    = ($page * $max_show) - $max_show;
    return [
        'limit_page' => $limit_page,
        'current'    => $current,
        "page"       => $page,
        'show'       => $max_show
    ];
}

    /**
     * Easy server validation
     * @param $type
     * @param $field
     * @return false|string
     */
function validationMethod($type,$field)
{
    $pattern = $type == 'login' || $type == 'password' ? "/^[a-z0-9_-]{4,16}$/" : "/^[A-z]{4,15}$/";
    if (!preg_match($pattern, $field)) {
        return 'Error, the '.$type.' must contain only letters and numbers, and be between 4 and 15 characters';
    } else {
        return false;
    }
}




