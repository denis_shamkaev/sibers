-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Сен 19 2020 г., 06:05
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sibers`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `rights` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `fname`, `lname`, `gender`, `date_of_birth`, `rights`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Denis', 'Shamkaev', 'Male', '1992-08-23', 1),
(2, 'Pool65', '21232f297a57a5a743894a0e4a801fc3', 'Peter', 'Poole                ', 'Male', '1989-02-06', NULL),
(3, 'Ruth233', '21232f297a57a5a743894a0e4a801fc3', 'Ruth', 'Wood', 'Fmale', '1992-03-17', NULL),
(4, 'KarenRoss', '21232f297a57a5a743894a0e4a801fc3', 'Karen', 'Ross', 'Fmale', '1990-01-01', NULL),
(5, 'Edward', '21232f297a57a5a743894a0e4a801fc3', 'Edward', 'Riley                ', 'Male', '1991-06-23', NULL),
(6, 'Adams1990', '9d4ee6dd564c9ac14eb6be02091a9224', 'Adams', 'Hall', 'Male', '1990-03-13', NULL),
(7, 'Alex24', '21232f297a57a5a743894a0e4a801fc3', 'Alexander', 'Harris', 'Male', '1982-08-13', NULL),
(8, 'HarveyAllen', '9c0ca0cabbd78a5a02bf8447347eced5', 'Allen', 'Harvey', 'Male', '1990-01-01', NULL),
(9, 'Janis18', 'dcc264d9d5ce6cdf4535b881de4add00', 'Janis', 'Campbell', 'Fmale', '1998-08-20', NULL),
(10, 'WattsW', '8139e3b2070343b1729402f9bf202052', 'Bonnie', 'Watts', 'Fmale', '1987-04-15', NULL),
(11, 'Mark94', 'b82a9a13f4651e9abcbde90cd24ce2cb', 'Mark', 'Baker', 'Male', '1994-02-01', NULL),
(12, 'Thomasina69', 'dcba3bf6586cb085d68853cd92a20f5a', 'Thomasina', 'Webster', 'Fmale', '1978-07-18', NULL),
(13, 'GeorgePS', '578ad8e10dc4edb52ff2bd4ec9bc93a3', 'George', 'Malone', 'Male', '1977-08-12', NULL),
(14, 'Anna', '97a9d330e236c8d067f01da1894a5438', 'Anna', 'Gardner', 'Fmale', '1992-07-19', NULL),
(15, 'GriffinVC', '5028efb14b76d3b8950efb37509a8cbf', 'Griffin', 'McLaughlin', 'Male', '1990-01-07', NULL),
(16, 'Betty', '7e754adda86a60e0a39d7204ba604243', 'Betty', 'Francis', 'Fmale', '1990-01-14', NULL),
(17, 'Cecil', '20f5da2d1273a9187f2302f73b59c4dd', 'Cecil', 'Kennedy', 'Fmale', '1990-09-11', NULL),
(18, 'Frank', 'f9dc77cece7fa16f6edd2d1d64853e4b', 'Frank', 'Anderson', 'Male', '1996-10-23', NULL),
(19, 'Dwayne', 'be04e966de67591bef33d62f45a65161', 'Dwayne', 'Goodwin', 'Male', '1990-01-17', NULL),
(20, 'Paul', 'c13e13da2073260c2194c15d782e86a9', 'Paul', 'Rice', 'Male', '1990-01-17', NULL),
(21, 'Jocelyn', '6a7e152235013c9c392fadd2c6aaddb2', 'Jocelyn', 'Holland', 'Fmale', '1985-02-12', NULL),
(22, 'Angelina', '1787f45088ea66fe34f974704abde7ff', 'Angelina', 'Townsend', 'Fmale', '1990-01-14', NULL),
(23, 'Thomas', '2042101ac1f6e7741bfe43f3672e6d7c', 'Morrison', 'Morrison', 'Male', '1995-04-05', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
