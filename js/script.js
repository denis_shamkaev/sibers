jQuery(document).ready(function ($) {

    $("#alert").hide();

    if ($.cookie('hide_modal')) {
        $("#cookies").hide();
    }

    if (!$.cookie('hide_modal')) {
        $("#cookies").fadeTo(5500, 1500).slideUp(500, function () {
            $("#cookies").slideUp(500);
        });
        $.cookie('hide_modal', 'true');
    }


    $("#login").submit(function () {
        const form   = $(this),
            validate = new FormData(this);
        validation(validate);
        $.ajax({
            url: '/',
            method: 'POST',
            data: form.serialize(),
            success: function (data) {
                data = jQuery.parseJSON(data);
                if (data.status === 'error') {
                    ShowAlert(data.text);
                    return false
                } else {
                    setLocation("/list");
                }
            }
        });
        return false;
    });


    $("#addUser").submit(function () {
        const form = $(this);
        validation_add_user(form);
        $.ajax({
            url: '/index.php',
            method: 'POST',
            data: form.serialize(),
            success: function (data) {
                console.log(data);
                data = jQuery.parseJSON(data);
                if (data.status === 'error') {
                    ShowAlert(data.text);
                    return false
                } else {
                    if (data.text.length > 1) {
                        $('#alert').removeClass('alert-danger').addClass('alert-success')
                        ShowAlert(data.text)
                    }

                    setTimeout(function () {
                        setLocation("/list");
                    }, 4000);
                    return false;
                }
            }
        });
        return false;
    });


    $("#updateUser").submit(function () {
        const form = $('#updateUser');
        validation_update_user();
        $.ajax({
            url: '/update',
            method: 'POST',
            data: form.serialize(),
            success: function (data) {
                data = jQuery.parseJSON(data);
                if (data.status === 'error') {
                    ShowAlert(data.text);
                    return false
                } else {
                    if (data.text.length > 1) {
                        $('#alert').removeClass('alert-danger').addClass('alert-success')
                        ShowAlert(data.text)
                    }
                    return false;
                }
            }
        });
        return false;
    });


    function setLocation(curLoc) {
        try {
            history.pushState(null, null, curLoc);
            location.reload();
            return;
        } catch (e) {
        }
        location.hash = '#' + curLoc;
    }


    function validation(form) {
        let message   = '',
            user_name = form.get('username'),
            password  = form.get('password');

        if (user_name.length < 5 || user_name.length > 15) {
            $("#user_name").addClass('error shake')
            message += 'Username';
        }

        if (password.length < 5 || user_name.length > 15) {
            $("#password").addClass('error shake')
            message += user_name.length < 5 || user_name.length > 15 ? ' and password' : 'Password';
        }

        if (password.length < 5 || user_name.length < 5) {
            message += ' is short';
            ShowAlert(message);
        } else if (password.length > 15 || user_name.length > 15) {
            message += ' is long';
            ShowAlert(message);
        }
        return false;
    }


    function validation_update_user() {
        let message   = '',
            user_name = $('#login').val(),
            fName     = $('#fname').val(),
            lName     = $('#lname').val();

        if (user_name.length < 5 || user_name.length > 15) {
            $("#login").addClass('error shake')
            message += 'Username';
        }

        if (fName.length < 5 || fName.length > 15) {
            $("#fname").addClass('error shake')
            message += lName.length < 5 || lName.length > 15 ? ', First Name' : ' First Name';
        }

        if (lName.length < 5 || lName.length > 15) {
            $("#lname").addClass('error shake')
            message += lName.length < 5 || lName.length > 15 ? ', Last Name' : ' Last Name';
        }

        if (user_name.length < 5 || fName.length < 5 || lName.length < 5) {
            message += ' is short';
            ShowAlert(message);
        } else if (user_name.length > 15 || fName.length > 15 || lName.length > 15) {
            message += ' is long';
            ShowAlert(message);
        }
        return false;
    }


    function validation_add_user() {
        let message   = '',
            user_name = $('#login').val(),
            password  = $('#password').val(),
            fName     = $('#fname').val(),
            lName     = $('#lname').val();

        if (user_name.length < 4 || user_name.length > 15) {
            $("#login").addClass('error shake')
            message += 'Username';
        }

        if (password.length < 4 || password.length > 15) {
            $("#password").addClass('error shake')
            message += lName.length < 4 || lName.length > 15 ? ', Password' : ' Password';
        }

        if (fName.length < 4 || fName.length > 15) {
            $("#fname").addClass('error shake')
            message += lName.length < 4 || lName.length > 15 ? ', First Name' : ' First Name';
        }

        if (lName.length < 4 || lName.length > 15) {
            $("#lname").addClass('error shake')
            message += lName.length < 5 || lName.length > 15 ? ', Last Name' : ' Last Name';
        }

        if (password.length < 4 || user_name.length < 4 || fName.length < 4 || lName.length < 4) {
            message += ' is short';
            ShowAlert(message);
        } else if (password.length > 15 || user_name.length > 15 || fName.length > 15 || lName.length > 15) {
            message += ' is long';
            ShowAlert(message);
        }
        return false;
    }

    function ShowAlert(message) {
        $('#alert-message').text(message);
        $("#alert").fadeTo(5500, 1500).slideUp(500, function () {
            $("#alert").slideUp(500);
        });
    }
});